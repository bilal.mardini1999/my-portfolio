import { Container, Row, Col, Tab, Nav } from "react-bootstrap";
import { ProjectCard } from "./ProjectCard";
import projImg1 from "../assets/img/project-img1.png";
import projImg2 from "../assets/img/project-img2.png";
import projImg3 from "../assets/img/project-img3.png";
import projImg4 from "../assets/img/project-img4.png";
import projImg5 from "../assets/img/project-img5.png";
import projImg6 from "../assets/img/project-img6.jpg";

import colorSharp2 from "../assets/img/color-sharp2.png";
import 'animate.css';
import TrackVisibility from 'react-on-screen';

export const Projects = () => {

  const projects = [
    {
      title: "Sa'ee",
      description: "is an application that aids drivers in delivering,sorting,and most importantly tracking their cargo",
      imgUrl: projImg1,
    },
    {
      title: "Fa Novels",
      description: "Detective is a platform that allows users to read decetive stories anywhere,anytime",
      imgUrl: projImg2,
    },
    {
      title: "Doctor Care",
      description: "Book appointments, purchase healthcare services, and shop for products all within the app. Experience the convenience of accessing comprehensive healthcare services ",
      imgUrl: projImg3,
    },
    {
      title: "PT Platform",
      description: "Get personalized workout plans, track progress, and connect with trainers all in one app",
      imgUrl: projImg4,
    },
    {
      title: "Amalia Dental Lab",
      description: "Our integrated system combines accounting, course management, and order tracking for dental labs. Streamline your operations, manage courses efficiently, and track orders seamlessly with Amalia Dental Lab.",
      imgUrl: projImg5,
    },
    {
      title: "Wafer La Gibak",
      description: "Explore, shop, and save with ease using Wafer La Gibak. Enjoy the convenience of online mall shopping, discover a variety of products from different retailers, and take advantage of exclusive promotions",
      imgUrl: projImg6,
    },
  ];

  return (
    <section className="project" id="projects">
      <Container>
        <Row>
          <Col size={12}>
            <TrackVisibility>
              {({ isVisible }) =>
              <div className={isVisible ? "animate__animated animate__fadeIn": ""}>
             <h2>Projects</h2>
<p>Explore a collection of my latest backend development projects, showcasing my proficiency in creating robust and efficient web applications.</p>
 <Tab.Container id="projects-tabs" defaultActiveKey="first">
                 
                  <Tab.Content id="slideInUp" className={isVisible ? "animate__animated animate__slideInUp" : ""}>
                    <Tab.Pane eventKey="first">
                      <Row>
                        {
                          projects.map((project, index) => {
                            return (
                              <ProjectCard
                                key={index}
                                {...project}
                                />
                            )
                          })
                        }
                      </Row>
                    </Tab.Pane>
                  
                  </Tab.Content>
                </Tab.Container>
              </div>}
            </TrackVisibility>
          </Col>
        </Row>
      </Container>
      <img className="background-image-right" src={colorSharp2}></img>
    </section>
  )
}
