import meter1 from "../assets/img/meter1.svg";
import meter2 from "../assets/img/meter2.svg";
import meter3 from "../assets/img/meter3.svg";
import cpp from "../assets/img/cpp.png";
import html from "../assets/img/html.png";
import js from "../assets/img/js.png";
import php from "../assets/img/php.png";
import css from "../assets/img/css.png";
import laravel from "../assets/img/laravel.png";
import github from "../assets/img/github.png";
import gitlab from "../assets/img/gitlab.png";
import cpanel from "../assets/img/cpanel.png";
import mysql from "../assets/img/mysql.png";
import postman from "../assets/img/postman.png";
import jquery from "../assets/img/jquery.png";
import ajax from "../assets/img/ajax.png";
import restapi from "../assets/img/restapi.png";
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import arrow1 from "../assets/img/arrow1.svg";
import arrow2 from "../assets/img/arrow2.svg";
import colorSharp from "../assets/img/color-sharp.png"

export const Skills = () => {
  const responsive = {
    superLargeDesktop: {
      // the naming can be any, depends on you.
      breakpoint: { max: 4000, min: 3000 },
      items: 5
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 3
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 2
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1
    }
  };

  return (
    <section className="skill" id="skills">
      <div className="container">
        <div className="row">
          <div className="col-12">
            <div className="skill-bx wow zoomIn">
              <h2>Skills</h2>
              <p>An outline of my diverse skillset, encompassing frontend and backend development<br></br> cloud platforms, and version control systems.</p>
              <Carousel responsive={responsive} infinite={true} className="owl-carousel owl-theme skill-slider">
                <div className="item">
                  <img src={laravel} alt="laravel" />

                </div>
                <div className="item">
                  <img src={restapi} alt="restapi" />

                </div>
                <div className="item">
                  <img src={php} alt="php" />

                </div>
                <div className="item">
                  <img src={postman} alt="postman" />

                </div>
                <div className="item">
                  <img src={cpp} alt="c++" />
                </div>
                <div className="item">
                  <img src={html} alt="html" className="html-img" />

                </div>
              
                <div className="item">
                  <img src={css} alt="css" />

                </div>
                <div className="item">
                  <img src={js} alt="js" />

                </div>
                <div className="item">
                  <img src={jquery} alt="jquery" />

                </div>

                <div className="item">
                  <img src={ajax} alt="ajax" />

                </div>
                <div className="item">
                  <img src={github} alt="github" />

                </div>
                <div className="item">
                  <img src={gitlab} alt="gitlab" />

                </div>
                <div className="item">
                  <img src={cpanel} alt="cpanel" />

                </div>
                <div className="item">
                  <img src={mysql} alt="mysql" />

                </div>
               
              
              </Carousel>
            </div>
          </div>
        </div>
      </div>
      <img className="background-image-left" src={colorSharp} alt="Image" />
    </section>
  )
}
