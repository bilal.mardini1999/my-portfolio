import { Container, Row, Col } from "react-bootstrap";
import { MailchimpForm } from "./MailchimpForm";
import navIcon1 from "../assets/img/nav-icon1.svg";
import navIcon2 from "../assets/img/nav-icon2.svg";
import navIcon3 from "../assets/img/nav-icon3.svg";
import navIcon4 from "../assets/img/nav-icon4.png";

export const Footer = () => {
  return (
    <footer className="footer">
      <Container>
        <Row className=" justify-content-center">
          <MailchimpForm />
          <Col size={12} sm={6} className="text-center ">
            <div className="social-icon-footer">
              <a target="_blank" href="www.linkedin.com/in/bilal-mardini-101695216"><img src={navIcon1} alt="Icon" /></a>
              <a target="_blank" href="https://www.facebook.com/bilal.mardini.79"><img src={navIcon2} alt="Icon" /></a>
              <a target="_blank" href="https://www.instagram.com/bilal.mardini_/"><img src={navIcon3} alt="Icon" /></a>
              <a target="_blank" href="https://api.whatsapp.com/send?phone=0969616331">
                <img src={navIcon4} alt="Icon" />
              </a>
            </div>
            <p>Copyright 2024. All Rights Reserved</p>
          </Col>
        </Row>
      </Container>
    </footer>
  )
}
